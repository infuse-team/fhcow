'use strict';

angular.module('wrtsApp')
//  .factory('Directions', [function(){
//        var DirectionsRequest = {
//            origin: patientAddress,
//            destination: "26 Queen St
//            Worcester, MA 01610",
//            travelMode: google.maps.TravelMode.TRANSIT,
//            transitOptions: {
//                arrivalTime: appointmentTime
//            },
//            unitSystem: UnitSystem.IMPERIAL,
//            provideRouteAlternatives: True,
//        };//  }])
  .controller('MainCtrl', function ($scope) {

//
        $scope.SubmitForm = function() {
            var StartingAddress = document.getElementById('patientAddress').value;
            var xmlHttp = null;

            xmlHttp = new XMLHttpRequest({mozSystem: true});
            xmlHttp.open("GET",  "https://script.google.com/macros/s/AKfycbxrN4XFgJJ90JhFnf-apeSyGLW_E-U_crBGba-QD2bk7U9SycM/exec?Starting="+StartingAddress, false);
            xmlHttp.send( null );
        };
    $scope.availableTimesAm = [
      {time:'9:30am'},
      {time:'10:00am'},
      {time:'10:30am'},
      {time:'11:00am'},
      {time:'11:30am'}
      ];
    $scope.availableTimesPm = [
      {time:'12:00pm'},
      {time:'12:30pm'},
      {time:'1:00pm'},
      {time:'1:30pm'},
      {time:'2:00pm'}
    ]
    $scope.availableDays = [
      {day:'Monday'},
      {day:'Tuesday'},
      {day:'Wednesday'},
      {day:'Thursday'},
      {day:'Friday'}
    ];


//    START OF GOOGLE MAPS API

    var directionsDisplay;
    var directionsService = new google.maps.DirectionsService();

//    Methods that need to be called once view loads
    $scope.initialize = function () {
      directionsDisplay = new google.maps.DirectionsRenderer();
      directionsDisplay.setPanel(document.getElementById('directions-panel'));
    }

    $scope.calcRoute = function(){

      var dateMoments = moment();
      var start = $scope.address;
      var end = "26 Queen St, Worcester, MA 01610";
      var appointmentTime = new Date(2014, 10);
      appointmentTime.setHours(13,30);
      var appointmentDay = $scope.modelDay;
      var appointmentHour = $scope.modelTime;
      var moments = moment(appointmentHour, "hh:mma").subtract(15, 'minutes');
      var dateMoments = new Date(moments);

//      Setting parameters of API Call
      var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.TRANSIT,
        transitOptions: {
          arrivalTime: dateMoments
        },
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        provideRouteAlternatives: true
      };

//      API Call
      directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      });
    }

    $scope.calcCurrentRoute = function(){

      var dateMoments = new Date(moment().subtract(15, 'minutes'));
      var start = $scope.address;
      var end = "26 Queen St, Worcester, MA 01610";
      var appointmentTime = new Date(2014, 10);
      appointmentTime.setHours(13,30);
      var appointmentDay = $scope.modelDay;
      var appointmentHour = $scope.modelTime;

//      Setting parameters of API Call
      var request = {
        origin: start,
        destination: end,
        travelMode: google.maps.TravelMode.TRANSIT,
        transitOptions: {
          arrivalTime: dateMoments
        },
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        provideRouteAlternatives: true
      };

//      API Call
      directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        }
      });
    }
//    END OF GOOGLE MAPS API

  });

